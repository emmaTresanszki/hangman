package hangman.example.com.hangman.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;

/**
 * Utilities and constants related to application preferences.
 * Created by emma.tresanszki on 10/18/2014.
 */
public class PrefUtils {

	/**
	 * String preference that holds the last played game's state.
	 */
	public static final String LAST_GAME_OVER = "last_game_state";

	/**
	 * String preferene that holds the ID of the last played game.
	 */
	public static final String LAST_GAME__ID = "last_game_id";

	/**
	 * Store the webservice ip.
	 */
	public static final String WEB_SERVICE_IP = "ip";

	/**
	 * Returns true if the last game was completed, false otherwise.
	 */
	public static boolean lastGameOver(final Context context) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		return sp.getBoolean(LAST_GAME_OVER, true);
	}

	/**
	 * Saves preference whether the last game was finished or not.
	 */
	public static void setLastGameOver(final Context context, boolean over) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		persist(sp.edit().putBoolean(LAST_GAME_OVER, over));
	}

	public static long lastGameId(final Context context) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		return sp.getLong(LAST_GAME__ID, Constants.INVALID_GAME_ID);
	}

	public static void setLastGameId(final Context context, final long id) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		persist(sp.edit().putLong(LAST_GAME__ID, id));
	}

	public static String getWebServiceIp(final Context context) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		return sp.getString(WEB_SERVICE_IP, "localhost");
	}

	public static void setWebServiceIp(final Context context, final String ip) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		persist(sp.edit().putString(WEB_SERVICE_IP, ip));
	}

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	private static void persist(final SharedPreferences.Editor editor) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
			editor.apply();
		} else {
			new Thread() {
				@Override
				public void run() {
					editor.commit();
				}
			}.start();
		}
	}
}