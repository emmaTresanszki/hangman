package hangman.example.com.hangman;

import android.app.Fragment;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import hangman.example.com.hangman.dialog.OptionDialogFragment;
import hangman.example.com.hangman.model.GameMove;
import hangman.example.com.hangman.model.GameRound;
import hangman.example.com.hangman.service.WebService;
import hangman.example.com.hangman.utils.Constants;
import hangman.example.com.hangman.utils.PrefUtils;


/**
 * A simple {@link Fragment} subclass.
 */
public class GameFragment extends Fragment
		implements View.OnClickListener, OptionDialogFragment.OnOptionDialogClickListener {
	private static String TAG = GameFragment.class.getSimpleName();
	private GameMoveTask moveTask;
	private GameResumeTask resumeTask;

	public GameFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_game, container, false);
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		createLetterButtons();

		if (PrefUtils.lastGameId(getActivity()) != Constants.INVALID_GAME_ID) {
			resumeTask = new GameResumeTask();
			resumeTask.execute(PrefUtils.lastGameId(getActivity()));
		}
	}

	private void createLetterButtons() {
		GridView keyboard = (GridView) getActivity().findViewById(R.id.gv_alphabet);

		Button letterBtn;
		ArrayList<Button> mButtons = new ArrayList<Button>();
		for (char buttonChar = 'A'; buttonChar <= 'Z'; buttonChar++) {
			// stylize the letter buttons.
			letterBtn = new Button(getActivity());
			letterBtn.setText("" + buttonChar);
			letterBtn.setId(buttonChar);
			letterBtn.setPadding(0, 0, 0, 0);
			letterBtn.setTextColor(getResources().getColor(R.color.white));
			letterBtn.setTextSize(getResources().getInteger(R.integer.game_letter_font_size));
			letterBtn.setOnClickListener(this);

			GradientDrawable gradientDrawable = new GradientDrawable();
			gradientDrawable.setColor(getResources().getColor(R.color.light_blue));
			gradientDrawable.setCornerRadius(getResources().getDimension(R.dimen.game_letter_radius));
			gradientDrawable.setStroke(
					getResources().getInteger(R.integer.letter_stroke),
					getResources().getColor(R.color.white));
			letterBtn.setBackgroundDrawable(gradientDrawable);
			//replace getActivity().getResources().getDrawable(R.drawable.button_style_pressed)

			mButtons.add(letterBtn);
		}
		keyboard.setAdapter(new LettersAdapter(mButtons));
	}

	@Override
	public void onClick(View view) {
		Button letterBtn = (Button) view;
		view.setVisibility(View.INVISIBLE);

		moveTask = new GameMoveTask();
		moveTask.execute(letterBtn.getText().toString().trim().toUpperCase());
	}

	@Override
	public void onOptionDialogPositiveClick(String tag) {
		Log.d(TAG, "onOptionDialogPositiveClick");

		getActivity().findViewById(R.id.tv_game_over_info).setVisibility(View.INVISIBLE);
		// Show the entire alphabet again.
		enableAlphabet();
		((GameActivity) getActivity()).updateGameZone(0);

		// Request for a new gameId ,and set the placeholder.
		((GameActivity) getActivity()).requestNewGame();
	}

	@Override
	public void onOptionDialogNegativeClick(String tag) {
	}

	/**
	 * Notify the UI of the game over state.
	 */
	public void notifyGameOver(boolean winState, String answer) {
		translateLetters(
				AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out), View.INVISIBLE);

		// Display game over message.
		TextView message = (TextView) getActivity().findViewById(R.id.tv_game_over_info);
		message.setTextColor(
				winState ?
						getResources().getColor(R.color.light_green) :
						getResources().getColor(R.color.light_pink));
		message.setText(winState ?
				getString(R.string.game_over_win_message) :
				getString(R.string.game_over_loose_message, answer.toUpperCase()));
		message.setVisibility(View.VISIBLE);

		displayGameOverDialog();
	}

	public void displayGameOverDialog() {
		android.app.Fragment dialogFragment =
				getFragmentManager().findFragmentByTag(OptionDialogFragment.TAG);

		if (dialogFragment == null) {
			dialogFragment = new OptionDialogFragment();
		}

		if (dialogFragment instanceof OptionDialogFragment && !dialogFragment.isAdded()) {
			dialogFragment.setTargetFragment(this, 0);
			((OptionDialogFragment) dialogFragment).show(getFragmentManager(), OptionDialogFragment.TAG);
		}
	}

	/**
	 * Animate the letters layout to bottom.
	 */
	private void translateLetters(Animation slide, int visibility) {
		View lettersLayout = getActivity().findViewById(R.id.gv_alphabet);
		if (lettersLayout != null) {

			lettersLayout.startAnimation(slide);
			lettersLayout.setVisibility(visibility);
		}
	}

	private void disableAlphabet() {
		GridView keyboard = (GridView) getActivity().findViewById(R.id.gv_alphabet);
		for (int i = 0; i < keyboard.getChildCount(); i++) {
			keyboard.getChildAt(i).setOnClickListener(null);
		}
	}

	private void enableAlphabet() {
		GridView keyboard = (GridView) getActivity().findViewById(R.id.gv_alphabet);
		for (int i = 0; i < keyboard.getChildCount(); i++) {
			keyboard.getChildAt(i).setVisibility(View.VISIBLE);
		}
		translateLetters(
				AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in), View.VISIBLE);
	}

	/**
	 * Notify of a network error while connecting to the backend.
	 */
	private void notifyConnectionError() {
		Handler handler = new Handler(getActivity().getMainLooper());
		handler.post(new Runnable() {
			public void run() {
				Toast.makeText(
						getActivity(), getActivity().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
			}
		});
	}

	/*** TODO delete this.Dummy game moves to play around until web service is live. **/
	private GameMove createDummyGameMove(String letter) {
		ArrayList<Integer> occurence = new ArrayList<Integer>(1);
		if (letter.equals("D")) {
			occurence.add(0);
			return new GameMove("D", occurence, false, null);
		} else if (letter.equals("O")) {
			occurence.add(1);
			return new GameMove("O", occurence, false, null);
		} else if (letter.equals("N")) {
			occurence.add(2);
			return new GameMove("N", occurence, false, null);
		} else if (letter.equals("U")) {
			occurence.add(3);
			return new GameMove("U", occurence, false, null);
		} else if (letter.equals("T")) {
			occurence.add(4);
			return new GameMove("T", occurence, true, "DONUT");
		} else if (letter.equals("X")) {
			return new GameMove("X", null, true, "DONUT");
		} else {
		}
		return new GameMove(letter, null, false, null);
	}

	/**
	 * Tasks that sends a new guess to the backend to retrieve a GameMove object.
	 */
	private class GameMoveTask extends AsyncTask<String, Void, GameMove> {
		private long gameId;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			gameId = PrefUtils.lastGameId(getActivity());
		}

		@Override
		protected GameMove doInBackground(String... params) {
			String letter = params[0];

//			return createDummyGameMove(letter);
			return WebService.computeMove(getActivity(), gameId, letter);
		}

		@Override
		protected void onPostExecute(GameMove result) {
			moveTask = null;

			if (result != null) {
				GameRound round = ((GameActivity) getActivity()).getGameRound();
				round.addGameMove(result);

				((GameActivity) getActivity()).updatePlaceholder(result);
				((GameActivity) getActivity()).updateGameZone(round.getWrongMovesCount());

				// If this was the last move, reset the gameId, so that this game round cannot be resumed.
				if (result.isGameOver()) {
					PrefUtils.setLastGameId(getActivity(), Constants.INVALID_GAME_ID);

					if (result.isMoveWrong()) {
						notifyGameOver(false, result.getAnswer());
					} else {
						notifyGameOver(true, result.getAnswer());
					}
				}
			} else {
				//game was not found anymore
				notifyConnectionError();
			}
		}

		@Override
		protected void onCancelled() {
			moveTask = null;
		}
	}

	/**
	 * Resumes the previous game, requesting all the game moves.
	 */
	private class GameResumeTask extends AsyncTask<Long, Void, GameRound> {
		private long gameId;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected GameRound doInBackground(Long... params) {
			gameId = params[0];

			//return ((GameActivity) getActivity()).createDummyGameRound(gameId);
			return WebService.resumeGame(getActivity(), params[0]);
		}

		@Override
		protected void onPostExecute(GameRound result) {
			resumeTask = null;

			if (result != null) {
				((GameActivity) getActivity()).initGame(result);

				for (GameMove move : result.getGameMoves()) {
					((GameActivity) getActivity()).updatePlaceholder(move);
					// hide played letters.
					GridView keyboard = (GridView) getActivity().findViewById(R.id.gv_alphabet);

					for (int i = 0; i < keyboard.getChildCount(); i++) {
						String letter = ((TextView) keyboard.getChildAt(i)).getText().toString().trim();

						if (letter.equals(move.getLetter())) {
							keyboard.getChildAt(i).setVisibility(View.INVISIBLE);
							keyboard.invalidate();
							break;
						}
					}
				}
			} else {
				notifyConnectionError();
			}
		}

		@Override
		protected void onCancelled() {
			resumeTask = null;
		}
	}

}
