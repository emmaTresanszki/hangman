package hangman.example.com.hangman;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import hangman.example.com.hangman.utils.Constants;
import hangman.example.com.hangman.utils.PrefUtils;


public class MainActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment())
					.commit();
		}
	}

	public void newGame(View view) {
		// no extra params needed, name GameId is requested.
		startActivity(new Intent(MainActivity.this, GameActivity.class));
	}

	/**
	 * Previous game is resumed.
	 * If there is no pending unfinished game, the Resume btn is not even accessible.
	 *
	 * @param view
	 */
	public void resumeGame(View view) {
		Intent intent = new Intent(MainActivity.this, GameActivity.class);
		intent.putExtra(Constants.INTENT_EXTRA_GAME_ID, PrefUtils.lastGameId(this));
		startActivity(intent);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button_style_default, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
								 Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container, false);
			return rootView;
		}

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);

			EditText ipEditText = (EditText) getActivity().findViewById(R.id.et_ip);
			ipEditText.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
				}

				@Override
				public void afterTextChanged(Editable s) {
					PrefUtils.setWebServiceIp(getActivity(), s.toString());
				}
			});

			updateResumeButton();
		}

		@Override
		public void onResume() {
			super.onResume();
			updateResumeButton();
		}

		private void updateResumeButton() {
			long lastGameId = PrefUtils.lastGameId(getActivity());
			Button resumeBtn =
					(Button) getActivity().findViewById(R.id.btn_resume_game);

			if (lastGameId == Constants.INVALID_GAME_ID) {
				resumeBtn.setVisibility(View.GONE);
			} else {
				resumeBtn.setVisibility(View.VISIBLE);
			}
		}
	}

}
