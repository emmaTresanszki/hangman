package hangman.example.com.hangman.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.os.Bundle;

import hangman.example.com.hangman.R;

/**
 * Created by emma.tresanszki on 10/12/2014.
 */
public class LoadDialogFragment extends DialogFragment {
	static public final String TAG = "dialog_loading";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setCancelable(false);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		ProgressDialog dialog = new ProgressDialog(getActivity());
		dialog.setMessage(getString(R.string.dialog_loading_message));

		return dialog;
	}
}
