package hangman.example.com.hangman;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import hangman.example.com.hangman.dialog.LoadDialogFragment;
import hangman.example.com.hangman.model.GameMove;
import hangman.example.com.hangman.model.GameRound;
import hangman.example.com.hangman.service.WebService;
import hangman.example.com.hangman.utils.Constants;
import hangman.example.com.hangman.utils.PrefUtils;


public class GameActivity extends BaseActivity {
	private static String TAG = GameActivity.class.getSimpleName();

	private long gameId = Constants.INVALID_GAME_ID;
	private GameRound gameRound;
	private NewGameTask newGameTask = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_game);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new GameFragment())
					.commit();
		}

		// Check if the game is resumed or not.
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			// This is the resumed state of the previous game.
			gameId = extras.getLong(Constants.INTENT_EXTRA_GAME_ID);
		} else {
			// request for a new gameId ,and set the placeholder.
			requestNewGame();
		}
	}

	/**
	 * Request for a new gameId ,and set the placeholder.
	 */
	public void requestNewGame() {
		this.gameRound = null;
		PrefUtils.setLastGameId(this, Constants.INVALID_GAME_ID);

		newGameTask = new NewGameTask();
		newGameTask.execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.play, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button_style_default, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * After the backend established the word, the placeholder can be displayed.
	 *
	 * @param wordLength
	 */
	public void setPlaceholder(int wordLength) {
		LinearLayout ll_placeholder = (LinearLayout) findViewById(R.id.ll_word);
		ll_placeholder.removeAllViews();

		TextView characterPlaceholder;
		ArrayList<TextView> mLabels = new ArrayList<TextView>();
		for (int i = 0; i < wordLength; i++) {
			characterPlaceholder = new TextView(this);
			characterPlaceholder.setGravity(Gravity.CENTER);
			characterPlaceholder.setSingleLine();
			characterPlaceholder.setTextColor(getResources().getColor(R.color.white));
			characterPlaceholder.setTextSize(getResources().getInteger(R.integer.placeholder_font_size));
			//characterPlaceholder.setTextScaleX(1.4f);
			characterPlaceholder.setTypeface(null, Typeface.BOLD);
			characterPlaceholder.setEms(1);
			characterPlaceholder.setMaxEms(1);
			characterPlaceholder.setMinEms(1);
			characterPlaceholder.setText(" _ ");
			characterPlaceholder.setId(i);

			mLabels.add(characterPlaceholder);
			ll_placeholder.addView(characterPlaceholder);
		}
	}

	/**
	 * Update the placeholder based on the latest move result.
	 *
	 * @param move
	 */
	public void updatePlaceholder(GameMove move) {
		ArrayList<Integer> occurrences = move.getOccurrences();

		// If the letter was found, display in on according positions.
		if (!move.isMoveWrong()) {
			LinearLayout ll_placeholder = (LinearLayout) findViewById(R.id.ll_word);
			TextView characterPlaceholder;

			for (int i = 0; i < occurrences.size(); i++) {
				characterPlaceholder = (TextView) ll_placeholder.getChildAt(occurrences.get(i));
				characterPlaceholder.setText(" " + move.getLetter().toUpperCase() + " ");
			}
		}
		// else if it was just wrong, display a toast message indicating moves left.
	}

	/**
	 * Update the drawing based on number of total wrong moves.
	 */
	public void updateGameZone(int wrongMoves) {
		ImageView hang_zone_iv = (ImageView) findViewById(R.id.iv_hang_zone);

		switch (wrongMoves) {
			case 0:
			default: {
				hang_zone_iv.setImageResource(R.drawable.step_0);
				break;
			}
			case 1: {
				hang_zone_iv.setImageResource(R.drawable.step_1);
				break;
			}
			case 2: {
				hang_zone_iv.setImageResource(R.drawable.step_2);
				break;
			}
			case 3: {
				hang_zone_iv.setImageResource(R.drawable.step_3);
				break;
			}
			case 4: {
				hang_zone_iv.setImageResource(R.drawable.step_4);
				break;
			}

		}
	}

	public GameRound getGameRound() {
		return this.gameRound;
	}

	public void showLoadingDialogFragment() {
		LoadDialogFragment dialogFragment = (LoadDialogFragment)
				getFragmentManager().findFragmentByTag(LoadDialogFragment.TAG);

		if (dialogFragment == null) {
			dialogFragment = new LoadDialogFragment();
		}

		dialogFragment.show(getFragmentManager(), LoadDialogFragment.TAG);
	}

	public void dismissLoadingDialogFragment() {
		LoadDialogFragment dialogFragment = (LoadDialogFragment)
				getFragmentManager().findFragmentByTag(LoadDialogFragment.TAG);

		if (dialogFragment != null) {
			dialogFragment.dismiss();
		}
	}

	/**
	 * Set the placeholder size for a new or resumed game round.
	 *
	 * @param result
	 */
	public void initGame(GameRound result) {
		GameActivity.this.gameRound = result;

		// Fill in the placeholder.
		setPlaceholder(result.getWordLength());

		if (result.getGameMoves() != null) {
			updateGameZone(result.getWrongMovesCount());
			Log.d(
					GameActivity.this.TAG + "Left moves: ",
					String.valueOf(Constants.MAX_MOVES - result.getGameMoves().size()));
		}
	}

	/*** TODO delete this.Dummy game moves to play around until web service is live. **/
	public GameRound createDummyGameRound(long gameId) {
		ArrayList<GameMove> moves = new ArrayList<GameMove>();
		ArrayList<Integer> posA = new ArrayList<Integer>(2);
		posA.add(0);
		posA.add(4);
		ArrayList<Integer> posL = new ArrayList<Integer>(1);
		posL.add(1);

		moves.add(new GameMove("A", posA, false, null));
		moves.add(new GameMove("X", null, false, null));
		moves.add(new GameMove("L", posL, false, null));

		return new GameRound(gameId, 5, moves);
	}

	/**
	 * Creates a new game task that requests the word length from the backend.
	 */
	private class NewGameTask extends AsyncTask<Void, Void, GameRound> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected GameRound doInBackground(Void... params) {
			//return new GameRound(1, 5, null);
			return WebService.startNewGame(GameActivity.this);
		}

		@Override
		protected void onPostExecute(GameRound result) {
			newGameTask = null;

			if (result != null) {
				initGame(result);

				// Persist the current game id.
				gameId = result.getGameId();
				PrefUtils.setLastGameId(GameActivity.this, gameId);
			} else {
				Handler handler = new Handler(getMainLooper());
				handler.post(new Runnable() {
					public void run() {
						Toast.makeText(GameActivity.this, getString(R.string.network_error), Toast.LENGTH_SHORT).show();
					}
				});
			}
		}

		@Override
		protected void onCancelled() {
			newGameTask = null;
		}
	}

}
