package hangman.example.com.hangman.utils;

/**
 * Constants used all across the app.
 * Created by emma.tresanszki on 10/18/2014.
 */
public class Constants {

	/**
	 * Used by game logic. The max nr of moves a user can make until the game is over.
	 */
	public static int MAX_MOVES = 4;
	public static long INVALID_GAME_ID = -1;

	/**
	 * Used for intent params
	 */
	public static String INTENT_EXTRA_GAME_ID = "gameId";

	/**
	 * Used for JSON node constant naming.
	 */
	public static String TAG_GAME_ID = "gameId";
	public static String TAG_WORD_LENGTH = "wordLength";
	public static String TAG_GAME_OVER = "gameOver";
	public static String TAG_GAME_MOVE_LETTER = "letter";
	public static String TAG_GAME_MOVE_POSITIONS = "positions";
	public static String TAG_GAME_ANSWER = "answer";
	public static String TAG_GAME_MOVE_RESPONSE_LIST = "moveResponseList";

}
