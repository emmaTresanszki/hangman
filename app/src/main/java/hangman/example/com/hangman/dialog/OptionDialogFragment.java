package hangman.example.com.hangman.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import hangman.example.com.hangman.R;

/**
 * The Game over dialog which allows the user to start a new game.
 * Created by emma.tresanszki on 10/14/2014.
 */
public class OptionDialogFragment extends DialogFragment implements DialogInterface.OnDismissListener {
	static public final String TAG = "option_dialog";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setCancelable(true);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog = super.onCreateDialog(savedInstanceState);
		dialog.setTitle(getResources().getString(R.string.game_over));

		return dialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.option_dialog, container, false);

		if (view != null) {
			View btPositive = view.findViewById(R.id.btn_new_game);
			btPositive.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Activity activity = getActivity();

					if (activity != null && activity instanceof OnOptionDialogClickListener) {
						((OnOptionDialogClickListener) activity)
								.onOptionDialogPositiveClick(getTag());
					}

					Fragment fragment = getTargetFragment();

					if (fragment != null && fragment instanceof OnOptionDialogClickListener) {
						((OnOptionDialogClickListener) fragment)
								.onOptionDialogPositiveClick(getTag());
					}

					dismiss();
				}
			});

			View btNegative = view.findViewById(R.id.btn_cancel);
			btNegative.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Activity activity = getActivity();

					if (activity != null && activity instanceof OnOptionDialogClickListener) {
						((OnOptionDialogClickListener) activity)
								.onOptionDialogNegativeClick(getTag());
					}

					Fragment fragment = getTargetFragment();

					if (fragment != null && fragment instanceof OnOptionDialogClickListener) {
						((OnOptionDialogClickListener) fragment)
								.onOptionDialogNegativeClick(getTag());
					}

					dismiss();
				}
			});

		}

		return view;
	}

	public interface OnOptionDialogClickListener {
		public void onOptionDialogPositiveClick(String tag);

		public void onOptionDialogNegativeClick(String tag);
	}

}
