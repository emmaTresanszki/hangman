package hangman.example.com.hangman.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import hangman.example.com.hangman.utils.Constants;

/**
 * The GameRound class holds the current game situation, from data retrieved from the server
 * as well as the user moves.
 * Created by emma.tresanszki on 10/12/2014.
 */
public class GameRound implements Parcelable {
	public static final Creator<GameRound> CREATOR = new Creator<GameRound>() {
		public GameRound createFromParcel(Parcel source) {
			return new GameRound(source);
		}

		public GameRound[] newArray(int size) {
			return new GameRound[size];
		}
	};
	ArrayList<GameMove> gameMoves = new ArrayList<GameMove>();
	private long gameId;
	private int wordLength;

	public GameRound(
			long gameId, int wordLength, ArrayList<GameMove> gameMoves) {
		this.gameId = gameId;
		this.wordLength = wordLength;
		this.gameMoves = new ArrayList<GameMove>();
		this.gameMoves = gameMoves;
	}

	private GameRound(Parcel in) {
		this.gameId = in.readLong();
		this.wordLength = in.readInt();
		this.gameMoves = (ArrayList<GameMove>) in.readSerializable();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(this.gameId);
		dest.writeInt(this.wordLength);
		dest.writeSerializable(this.gameMoves);
	}

	public int getWrongMovesCount() {
		int wrongMovesCount = 0;
		for (GameMove move : this.gameMoves) {
			if (move.isMoveWrong()) {
				wrongMovesCount++;
			}
		}
		return wrongMovesCount;
	}

	public int getMovesLeft() {
		return Constants.MAX_MOVES - getWrongMovesCount();
	}

	public int getWordLength() {
		return wordLength;
	}

	public void setWordLength(int wordLength) {
		this.wordLength = wordLength;
	}

	public long getGameId() {
		return gameId;
	}

	public void setGameId(long gameId) {
		this.gameId = gameId;
	}

	public ArrayList<GameMove> getGameMoves() {
		return this.gameMoves;
	}

	public void setGameMoves(ArrayList<GameMove> gameMoves) {
		this.gameMoves = gameMoves;
	}

	public void addGameMove(GameMove move) {
		if (this.gameMoves == null) {
			this.gameMoves = new ArrayList<GameMove>();
		}
		this.getGameMoves().add(move);
	}

	@Override
	public int describeContents() {
		return 0;
	}
}
