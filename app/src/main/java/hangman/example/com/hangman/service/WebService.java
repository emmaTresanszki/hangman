package hangman.example.com.hangman.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;

import hangman.example.com.hangman.R;
import hangman.example.com.hangman.model.GameMove;
import hangman.example.com.hangman.model.GameRound;
import hangman.example.com.hangman.utils.Constants;
import hangman.example.com.hangman.utils.PrefUtils;

/**
 * Created by emma.tresanszki on 10/18/2014.
 */
public class WebService extends Service {
	private static final String TAG = WebService.class.getSimpleName();

	private final IBinder binder = new WebServiceBinder();

	/**
	 * Connects to the backend, requesting for a new game to start.
	 *
	 * @return GameRound that contains: The new game unique identifier and the word length.
	 */
	public static GameRound startNewGame(Context context) {
		Log.d(TAG, "startNewGame");

		URI uri = URI.create(
				context.getString(R.string.general_endpoint, PrefUtils.getWebServiceIp(context)) +
						"startNewGame");
		HttpGet httpGet = new HttpGet(uri);
		httpGet.setHeader("Content-Type", "application/json");

		DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
		try {
			HttpResponse response = defaultHttpClient.execute(httpGet);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				HttpEntity httpEntity = response.getEntity();
				JSONObject json = new JSONObject(EntityUtils.toString(httpEntity));
				Log.d(TAG, "startNewGame response: " + json.toString());

				return new GameRound(
						json.getLong(Constants.TAG_GAME_ID),
						json.getInt(Constants.TAG_WORD_LENGTH),
						null);
			}
		} catch (IOException e) {
			Log.e(TAG, "startNewGame - IOException", e);
		} catch (JSONException e) {
			Log.e(TAG, "startNewGame - JSONException", e);
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "computeMove - IllegalArgumentException", e);
		}

		return null;
	}

	public static GameMove computeMove(Context context, final long gameId, final String letter) {
		Log.d(TAG, "computeMove");

		URI uri = URI.create(
				context.getString(R.string.general_endpoint, PrefUtils.getWebServiceIp(context)) +
						"computeMove?gameId=" + gameId + "&letter=" + letter);
		HttpGet httpGet = new HttpGet(uri);
		httpGet.setHeader("Content-Type", "application/json");

		DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
		try {
			HttpResponse response = defaultHttpClient.execute(httpGet);

			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				HttpEntity httpEntity = response.getEntity();
				JSONObject json = new JSONObject(EntityUtils.toString(httpEntity));
				Log.d(TAG, "computeMove response: " + json.toString());

				ArrayList<Integer> occurrrences = null;
				if (json.has(Constants.TAG_GAME_MOVE_POSITIONS)) {
					JSONArray positions = json.getJSONArray(Constants.TAG_GAME_MOVE_POSITIONS);
					occurrrences = new ArrayList<Integer>(positions.length());

					for (int i = 0; i < positions.length(); i++) {
						occurrrences.add(positions.getInt(i));
					}
				}

				return new GameMove(
						json.getString(Constants.TAG_GAME_MOVE_LETTER),
						occurrrences,
						json.getBoolean(Constants.TAG_GAME_OVER),
						json.getString(Constants.TAG_GAME_ANSWER));
			}
		} catch (IOException e) {
			Log.e(TAG, "computeMove - IOException", e);
		} catch (JSONException e) {
			Log.e(TAG, "computeMove - JSONException", e);
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "computeMove - IllegalArgumentException", e);
		}

		return null;
	}

	public static GameRound resumeGame(Context context, final long gameId) {
		Log.d(TAG, "resumeGame");

		URI uri = URI.create(
				context.getString(R.string.general_endpoint, PrefUtils.getWebServiceIp(context)) +
						"resumeGame?gameId=" + gameId);
		HttpGet httpGet = new HttpGet(uri);
		httpGet.setHeader("Content-Type", "application/json");

		DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
		try {
			HttpResponse response = defaultHttpClient.execute(httpGet);

			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				HttpEntity httpEntity = response.getEntity();
				JSONObject json = new JSONObject(EntityUtils.toString(httpEntity));
				Log.d(TAG, "resumeGame response:" + json.toString());

				ArrayList<GameMove> moves = new ArrayList<GameMove>();
				GameMove gameMove;
				ArrayList<Integer> occurrences = new ArrayList<Integer>();

				JSONArray movesArray = json.getJSONArray(Constants.TAG_GAME_MOVE_RESPONSE_LIST);
				for (int i = 0; i < movesArray.length(); i++) {
					JSONObject moveJson = movesArray.getJSONObject(i);

					if (moveJson.has(Constants.TAG_GAME_MOVE_POSITIONS)) {
						JSONArray positions = moveJson.getJSONArray(Constants.TAG_GAME_MOVE_POSITIONS);
						occurrences = new ArrayList<Integer>(positions.length());

						for (int pos = 0; pos < positions.length(); pos++) {
							occurrences.add(positions.getInt(pos));
						}
					}

					gameMove = new GameMove(moveJson.getString(Constants.TAG_GAME_MOVE_LETTER),
							occurrences,
							moveJson.getBoolean(Constants.TAG_GAME_OVER),
							moveJson.getString(Constants.TAG_GAME_ANSWER));

					moves.add(gameMove);
				}

				return new GameRound(
						gameId,
						json.getInt(Constants.TAG_WORD_LENGTH),
						moves);
			}
		} catch (IOException e) {
			Log.e(TAG, "resumeGame - IOException", e);
		} catch (JSONException e) {
			Log.e(TAG, "resumeGame - JSONException", e);
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "computeMove - IllegalArgumentException", e);
		}

		return null;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return START_STICKY;
	}

	public class WebServiceBinder extends Binder {
		public WebService getService() {
			return WebService.this;
		}
	}
}
