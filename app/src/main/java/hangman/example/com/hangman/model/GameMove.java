package hangman.example.com.hangman.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * GameMove encapsulates the backend response after a user took a guess.
 * Created by emma.tresanszki on 10/19/2014.
 */
public class GameMove implements Parcelable {
	public static final Creator<GameMove> CREATOR = new Creator<GameMove>() {

		public GameMove createFromParcel(Parcel source) {
			return new GameMove(source);
		}

		public GameMove[] newArray(int size) {
			return new GameMove[size];
		}
	};
	private String letter;
	private ArrayList<Integer> occurrences;
	private boolean gameOver;
	// The answer is available only if the game is over.
	private String answer;

	public GameMove(String letter, ArrayList<Integer> occurences, boolean gameOver, String answer) {
		this.letter = letter;
		this.occurrences = occurences;
		this.gameOver = gameOver;
		this.answer = answer;
	}

	private GameMove(Parcel in) {
		this.letter = in.readString();
		this.occurrences = (ArrayList<Integer>) in.readSerializable();
		this.gameOver = in.readInt() == 1 ? true : false;
		this.answer = in.readString();
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getLetter() {
		return letter;
	}

	public void setLetter(String letter) {
		this.letter = letter;
	}

	public boolean isMoveWrong() {
		return (this.occurrences.size() == 0 ? true : false);
	}

	public ArrayList<Integer> getOccurrences() {
		return occurrences;
	}

	public void setOccurrences(ArrayList<Integer> occurences) {
		this.occurrences = occurences;
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.letter);
		dest.writeSerializable(this.occurrences);
		dest.writeInt(this.gameOver ? 1 : 0);
		dest.writeString(this.answer);
	}

	@Override
	public int describeContents() {
		return 0;
	}
}

