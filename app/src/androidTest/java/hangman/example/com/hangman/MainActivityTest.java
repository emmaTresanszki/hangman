package hangman.example.com.hangman;

import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;
import android.view.View;
import android.widget.Button;

/**
 * Created by emma.tresanszki on 10/27/2014.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {
	private final static String DUMMY_LABEL_TEXT = "dummy";

	MainActivity activity;
	Button resumeGame;


	public MainActivityTest() {
		super(MainActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();

		activity = getActivity();
		resumeGame = (Button) activity.findViewById(R.id.btn_resume_game);
	}

	@UiThreadTest
	public void testPauseState() {
		resumeGame.setText(DUMMY_LABEL_TEXT);
		this.getInstrumentation().callActivityOnPause(activity);
		resumeGame.setText(activity.getString(R.string.resume_game));
		this.getInstrumentation().callActivityOnResume(activity);

		assertEquals(
				activity.getString(R.string.resume_game),
				resumeGame.getText().toString());
	}

	public void testPreconditions() {
		assertNotNull("activity is null", activity);
		assertNotNull("resumeGame is null", resumeGame);
	}

	public void testVisibility_resumeBtn() {
		assertEquals(View.VISIBLE, resumeGame.getVisibility());
	}

	public void testText_resumeBtn() {
		assertEquals(
				activity.getString(R.string.resume_game),
				resumeGame.getText().toString()
		);
	}

}
