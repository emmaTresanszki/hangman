package hangman.example.com.hangman;

import android.test.AndroidTestCase;

import hangman.example.com.hangman.model.GameMove;


public class GameMoveTest extends AndroidTestCase {
	private static final String LETTER = "A";
	private GameMove gameMove;


	@Override
	protected void setUp() throws Exception {
		super.setUp();
		gameMove = new GameMove(LETTER, null, false, null);
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		this.gameMove = null;
	}

	public void testPreconditions() {
		assertNotNull("testPreconditions fail, null object", gameMove);
	}

	public void test_letter() throws Exception {
		assertEquals(LETTER, gameMove.getLetter());

	}
}