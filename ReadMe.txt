  ------------------- Hangman client app -------------------
	This app is for demo purposed only, it does not provide the best solution, while the main logic is computed on the
back-end side.
The source code for the web service can be cloned from https://bitbucket.org/emmaTresanszki/hangmanwebservice
Tested on: HTC One S and Samsung GALAXY Tab2, Signed with debug key.
The client application is built in Android Studio Beta (0.8.9)

	The network requests are simplified as much as possible, such that the jSONs can be handled fast, without slowing
down the app. At this stage there is no user authentication done, as the game instances are mapped only through unique
gameIds. The dictionary contains android version code names.

NOTE:
	For testing without installing Android SDK, there is the latest apk file available under apks\ .

Game Logic:
	- The app is launched the first time, therefore there can be no pending gameId. In this case a new game can be
	created. A new game request retrieves two things: the gameId and the word length.
	- Based on an already created GameRound each user move sends away: the gameId and the letter. The GameMove response
	contains: the played letter, int array of occurrences(or null), isGameOver state and the answer (null if it's not
	game over).
	- At this stage	MAX_WRONG_MOVES is set to 4, such that the hangman is drawn in 4 steps.
	- Once the game is over a dialog pops allowing the user to start over.
	- The app is resumed/reopened and there is a pending game that needs to be restored. In this case resumeGame(@param
	gameId) is called to retrieve the list of GameMoves played so far, and update the UI accordingly.

For a real-life app, the user authentication would be mandatory to safely distinguish games, and keep the history.

	 ------------------- Future features  -------------------
	- Authentication, at least with a unique deviceId. User account, user authentication and high scores.
	- Add categories.
	- Add game level: difficult, medium, easy ( which mean some hint letters are given).
	- The maximum number of wrong moves could be established by the server for each GameRound. This mean the hangman
	drawing steps could be dynamically set.
	- Add hints, for instance images if the app is designed for kids.
